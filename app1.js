const http = require('http')
const fs = require('fs')
const express = require('express')
const app = express();
const PORT=8000;

app.use(express.static(__dirname + '/Views'));

app.get('/about', (req, res)=>{
    res.redirect('about.html');
});

app.get('/contact', function(req, res){
    res.redirect('contact.html');
});

app.listen(PORT,()=>{
    console.log("Server Listening on port 3000");
})