const http = require('http')
const fs = require('fs')

const onRequest = (request, response) =>{
    console.log(request.url, request.method);
    let path ='./Views/';
    if(request.url==='/'){
        path= path+'index.html'
        response.statusCode=200;
    }
    else if(request.url==='/about'){
        path= path+'about.html'
        response.statusCode=200;
    }
    else if(request.url==='/contact'){
        path= path+'contact.html'
        response.statusCode=200;
    }
    else{
        path=path+'404.html'
        response.statusCode=400;
    }

    //set header
    response.setHeader("Content-Type","text/html")
    //send
    fs.readFile(path,null,(err,data) => {
        if(err){
            response.writeHead(400)
            response.write("File Not Found")
        }
        else{
            response.write(data)
        }
        response.end()
    })
}

http.createServer(onRequest).listen(8000,'localhost',()=>{
    console.log('Listening for request on port 8000')
})